document.addEventListener("DOMContentLoaded", function(event) { 
  console.log('Document is Ready.');

  new Vue({
    el: '#navbar-vue-app',
    data: {
      isOpenMenu: false,
      isOpenSubmenu: false
    },
    methods: {
      toggleOpenMenu: function (event) {
        this.isOpenMenu = !this.isOpenMenu;
        console.log('Toggle show class in navbar burger menu.');
      },
      toggleOpenSubmenu: function (event) {
        this.isOpenSubmenu = !this.isOpenSubmenu;
        console.log('Toggle show class in dropdown submenu.');
      },
      onResize(event) {
        if (this.isOpenSubmenu) {
          console.log('Remove show class before collapsing.');
          this.isOpenSubmenu = false;
        }
      }
    },
    mounted() {
      // Register an event listener when the Vue component is ready
      window.addEventListener('resize', this.onResize)
    },
    beforeDestroy() {
      // Unregister the event listener before destroying this Vue instance
      window.removeEventListener('resize', this.onResize)
    }
  });

});
