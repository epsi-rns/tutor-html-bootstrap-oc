document.addEventListener("DOMContentLoaded", function(event) { 

  const leftToggler  = document.getElementById("left_toggler");
  const rightToggler = document.getElementById("right_toggler");

  /*
  element.onclick = function() {
  }

  element.addEventListener("click", function(){
  }); 

  element.addEventListener("click", () => {
  }); 
  */

  leftToggler.addEventListener("click", () => {
    leftToggler.classList.toggle("left_toggler_active");
 
    toggleLeftIcon();
    toggleLeftLayout(); 
    fixGapAndEdge();

    leftToggler.blur();
    return false;
  });

  rightToggler.addEventListener("click", () => {
    rightToggler.classList.toggle("right_toggler_active");

    toggleRightIcon();
    toggleRightLayout();
    fixGapAndEdge();

    rightToggler.blur();
    return false;
  });

  function toggleLeftIcon() {
    const isActiveLeft = leftToggler.classList
      .contains("left_toggler_active");
   
    const leftIcon = leftToggler
      .getElementsByTagName("img")[0];

    if (isActiveLeft) {
      leftIcon.src = "assets/icons/chevrons-left.svg";
      console.log("left toggler class is active");
    } else {
      leftIcon.src = "assets/icons/chevrons-right.svg"
      console.log("left toggler class is inactive");
    }
  }

  function toggleRightIcon() {
    const isActiveRight = rightToggler.classList
      .contains("right_toggler_active");

    const rightIcon = rightToggler
      .getElementsByTagName("img")[0];
  
    if (isActiveRight) {
      rightIcon.src = "assets/icons/chevrons-right.svg"      
      console.log("right toggler class is active");
    } else {
      rightIcon.src = "assets/icons/chevrons-left.svg"
      console.log("right toggler class is inactive");
    }
  }

  // Toggle maxwidth feature class
  // Enable/disable to use full width wide screen
  function toggleLeftLayout() {
    const maxWidthTogglers = document
      .getElementsByClassName("maxwidth_toggler");

    // ECMAScript 2015 
    for (let mwt of maxWidthTogglers) {
      mwt.classList.toggle("maxwidth");
    }
  }

  // Toggle sidebar
  // Enable/disable full width of content on tablet screen or beyond
  function toggleRightLayout() {
    const isActiveRight = rightToggler.classList
      .contains("right_toggler_active");

    const mainToggler  = document.getElementById("main_toggler");
    const mainTogglerW = document.getElementById("main_toggler_wrapper");
    const asideToggler = document.getElementById("aside_toggler");

    if (isActiveRight) {
      mainToggler.classList.add("col-md-8");
      mainToggler.classList.remove("col-md-12");

      mainTogglerW.classList.remove("single");

      asideToggler.classList.remove("d-none"); 
      asideToggler.classList.remove("d-sm-block");
      asideToggler.classList.remove("d-md-none");
    } else {
      mainToggler.classList.add("col-md-12");
      mainToggler.classList.remove("col-md-8");

      mainTogglerW.classList.add("single");

      asideToggler.classList.add("d-none");
      asideToggler.classList.add("d-sm-block");
      asideToggler.classList.add("d-md-none");
    }
  }

  // Fix gap between two columns
  function fixGapAndEdge() {
    const isActiveLeft = leftToggler.classList
      .contains("left_toggler_active");
    const isActiveRight = rightToggler.classList
      .contains("right_toggler_active");

    const mainToggler  = document.getElementById("main_toggler");
    const asideToggler = document.getElementById("aside_toggler");

    // Fix Gap
    mainToggler.classList.remove("pr-0");
    asideToggler.classList.remove("pl-0");

    if (!isActiveLeft && isActiveRight) {
      mainToggler.classList.add("pr-0");
      asideToggler.classList.add("pl-0");
    }

    // Fix Edge
    if (isActiveLeft) {
      mainToggler.classList.add("px-0");
      asideToggler.classList.add("px-0");
    } else {
      mainToggler.classList.remove("px-0");
      asideToggler.classList.remove("px-0");
    }

    console.log("Fix gap and edge class.");
  }

});
