# Bootstrap OC

A Bootstrap Open Color Experiment.

> Pure HTML5 + Bootstrap CSS + Open Color + Custom SASS

## Experimental

This repository might change from time to time.

-- -- --

## Links

### Bootstrap OC

This repository:

* [Bootstrap Open Color Step by Step Repository][tutorial-bootstrap-oc]

### Comparation

Comparation with other guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md]

* [Materialize Step by Step Repository][tutorial-materialize]

* [Semantic UI Step by Step Repository][tutorial-semantic-ui]

### Eleventy Step By Step

Using Bootstrap Open Color in SSG:

* Soon

[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-bootstrap-oc]:https://gitlab.com/epsi-rns/tutor-html-bootstrap-occ/
[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/
[tutorial-semantic-ui]: https://gitlab.com/epsi-rns/tutor-html-semantic-ui/

-- -- --

Open Color start from step 04.
Step 01 until step 03 contain Bootstrap preparation.

## Step 01

> Adding Bootstrap CSS

* Without Bootstrap

* Using CDN

* Using Local

![screenshot][screenshot-01]

## Step 02

> Using Bootstrap CSS: Navigation Bar

* From Simple to Full Featured

* Javascript: With jQuery, With Vue JS, Plain Javascript.

* Icons: FontAwesome, Boostrap Icon, Feathers Icon.

![screenshot][screenshot-02]

## Step 03

> Custom SASS

* Custom maximum width class

* Simple responsive layout using Bootstrap

![screenshot][screenshot-03]

## Step 04

> Open Color

* Open Color Demo, using tailor made sass

* Double column demo, responsive gap using custom sass

* Single column demo, simple landing page

![screenshot][screenshot-04]

## Step 05

> HTML Box using Open Color

* Main Blog and Aside Widget

![screenshot][screenshot-05]

## Step 06

> Finishing

* Blog Post Example

![screenshot][screenshot-06]

-- -- --

| Disclaimer | Use it at your own risk |
| ---------- | ----------------------- |

[screenshot-01]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-01/html-bootstrap-oc-preview.png
[screenshot-02]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-02/html-bootstrap-oc-preview.png
[screenshot-03]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-03/html-bootstrap-oc-preview.png
[screenshot-04]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-04/html-bootstrap-oc-preview.png
[screenshot-05]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-05/html-bootstrap-oc-preview.png
[screenshot-06]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-06/html-bootstrap-oc-preview.png
